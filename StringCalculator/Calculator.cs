﻿using StringCalculator.Functionality;
using StringCalculator.Source;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            Addition addition = new Addition(); 
            Number number = new Number();
            Delimiter delimiter = new Delimiter();

            string numberSections = number.GetNumberSection(numbers);
            string[] delimiters = delimiter.GetDelmiters(numbers);
            List<int> numbersList = number.GetNumbers(numberSections, delimiters);

            number.ValidateNumbers(numbersList);

            return addition.GetSum(numbersList);
        }
    }
}
