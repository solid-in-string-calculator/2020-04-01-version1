﻿using System;

namespace StringCalculator.Source
{
    public class Delimiter
    {
        public string[] GetDelmiters(string numbers)
        {
            var customDelmitersId = "//";
            var newline = "\n";
            var multipleCustomDelimitersId = $"{customDelmitersId}[";
            var multipleCustomDelimitersSeperator = $"]{newline}";
            var multipleCustomDelimitersSplitter = "][";

            if (numbers.StartsWith(multipleCustomDelimitersId))
            {
                string delmiters = numbers.Substring(numbers.IndexOf(multipleCustomDelimitersId) + multipleCustomDelimitersId.Length, numbers.IndexOf(multipleCustomDelimitersSeperator) - (multipleCustomDelimitersSeperator.Length + 1));

                return delmiters.Split(new[] { multipleCustomDelimitersSplitter}, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(customDelmitersId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelmitersId) + customDelmitersId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", newline };
        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }
}
